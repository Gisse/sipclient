package google.android.sipclient;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.sip.SipAudioCall;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Data;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CallScreen extends Activity{
	
	private static final String TAG = "sipclient";

	public String sipAddress = null;

    public SipManager manager = null;
    public SipProfile me = null;
    public SipAudioCall call = null;
    
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.v(TAG, "Activity State: onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.callscreen);
        final Button cancelButton = (Button) findViewById(R.id.callscreen_cancelcall);
        if(isMyServiceRunning()){
        	final Intent monitorActivity = new Intent(this, Monitor.class);
        	startService(monitorActivity);
        }else{
        	Log.v(TAG, "Service runing skiping");
        }
        Bundle extras = getIntent().getExtras();
        getAdress(extras.getLong("user_ID"));
        initiateCall();
        
        
        cancelButton.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
            	Log.v(TAG, "Creating new intent for Contacts");
            	closeCall();
        		Intent intent = new Intent();
        		intent.setClassName("google.android.sipclient", "google.android.sipclient.Contacts");
        		startActivity(intent);
            }
        });
    }
	
	private boolean isMyServiceRunning() {
	    ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (".Monitor".equals(service.service.getClassName())) {
	        	Log.v(TAG, "Starting service as it is not started");
	            return true;
	        }
	    }
	    Log.v(TAG, "Service already working");
	    return false;
	}
	
	/**
     * Make an outgoing call.
     */
    public void initiateCall() {
    	Log.v(TAG, "Inicializing call");
        updateStatus(sipAddress);

        try {
            SipAudioCall.Listener listener = new SipAudioCall.Listener() {
                // Much of the client's interaction with the SIP Stack will
                // happen via listeners.  Even making an outgoing call, don't
                // forget to set up a listener to set things up once the call is established.
                @Override
                public void onCallEstablished(SipAudioCall call) {
                    call.startAudio();
                    call.setSpeakerMode(true);
                    call.toggleMute();
                    updateStatus(call);
                }

                @Override
                public void onCallEnded(SipAudioCall call) {
                    updateStatus("Ready.");
                }
            };
            if (sipAddress != null){
            	call = manager.makeAudioCall(me.getUriString(), sipAddress, listener, 30);
            }else{
            	Toast.makeText(this, "No adress???", Toast.LENGTH_LONG).show();
            }

        }
        catch (Exception e) {
            Log.i("WalkieTalkieActivity/InitiateCall", "Error when trying to close manager.", e);
            closeCall();
        }
    }
    
    /**
     * Updates the status box with the SIP address of the current call.
     * @param call The current, active call.
     */
    public void updateStatus(SipAudioCall call) {
        String useName = call.getPeerProfile().getDisplayName();
        if(useName == null) {
          useName = call.getPeerProfile().getUserName();
        }
        updateStatus(useName + "@" + call.getPeerProfile().getSipDomain());
    }
    
    /**
     * Updates the status box at the top of the UI with a messege of your choice.
     * @param status The String to display in the status box.
     */
    public void updateStatus(final String status) {
        // Be a good citizen.  Make sure UI changes fire on the UI thread.
        this.runOnUiThread(new Runnable() {
            public void run() {
                TextView labelView = (TextView) findViewById(R.id.callscreen_callername);
                labelView.setText(status);
            }
        });
    }
    
    public void getAdress(long l){
    	String address = null;
    	Log.v(TAG, "We got this ID: "+l);
        Cursor c = getContentResolver().query(Data.CONTENT_URI,
                new String[] {CommonDataKinds.SipAddress.SIP_ADDRESS},
                Data.CONTACT_ID + "=?" + " AND "
                        + Data.MIMETYPE + "='" + CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE + "'",
                new String[] {String.valueOf(l)}, null);
        Log.v(TAG, "Povukli podatke iz baze, sada ih citamo");
        while (c.moveToNext()) {
        	Log.v(TAG, "Usli u while");
        	address = c.getString(
        			c.getColumnIndex(ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS));
        }
        Log.v(TAG, "iz<asli iz while i zatvaramo kursor");
        c.close();
        Log.v(TAG, "We got this sip addres: "+address);
    	sipAddress = address;
    }
    
    public void closeCall(){
    	Log.v(TAG, "closing call");
    	if (me != null) {
            try {
                manager.close(me.getUriString());
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }
        if (call != null) {
        	Log.v(TAG, "call closed");
            call.close();
        }
    }

}
