package google.android.sipclient;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public final class Contacts extends Activity
{

	private static final String TAG = "sipclient";

    private Button mAddAccountButton;
    private ListView mContactList;
    private boolean mShowInvisible;
    private CheckBox mShowInvisibleControl;

    /**
     * Called when the activity is first created. Responsible for initializing the UI.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.v(TAG, "Activity State: onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts);

        // Obtain handles to UI objects
        mAddAccountButton = (Button) findViewById(R.id.addContactButton);
        mContactList = (ListView) findViewById(R.id.contactList);
        mShowInvisibleControl = (CheckBox) findViewById(R.id.showInvisible);

        // Initialize class properties
        mShowInvisible = false;
        mShowInvisibleControl.setChecked(mShowInvisible);

        //Contakt kliknut kratko, poziv se odradjuje
        mContactList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Log.d(TAG, "Nesto kliknuto???");
				Log.d(TAG, "ID kliknutog:"+arg3);
				Intent intent = new Intent();
        		intent.setClassName("google.android.sipclient", "google.android.sipclient.CallScreen");
        		intent.putExtra("user_ID", arg3);
        		startActivity(intent);
			}
        	
		});
        //Kontakt se drzi, odradjuje se edit kontakta
        mContactList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Log.d(TAG, "Nesto se drzi???");
				Log.d(TAG, "ID kliknutog:"+arg3);
				Log.v(TAG, "Creating new intent for Contacts");
        		/*Intent intent = new Intent();
        		intent.putExtra("contactID", arg3);
        		intent.setClassName("google.android.sipclient", "google.android.sipclient.ContactEditor");
        		startActivity(intent);
				Intent newIntent = new Intent(Intent.ACTION_INSERT, 
						ContactsContract.Contacts.CONTENT_URI); 
						startActivityForResult(newIntent, 1); */
				Intent i = new Intent(Intent.ACTION_EDIT);
				i.setData(Uri.parse(ContactsContract.Contacts.CONTENT_LOOKUP_URI + "/" + arg3));
				startActivityForResult(i, Menu.FIRST);
				return false;
			}
		});
        
        // Register handler for UI elements
        //Stisnuto Dugme za dodavanje kontakta
        mAddAccountButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "mAddAccountButton clicked");
                //launchContactAdder();
                Intent newIntent = new Intent(Intent.ACTION_INSERT, 
						ContactsContract.Contacts.CONTENT_URI); 
						startActivityForResult(newIntent, 1);
            }
        });
        
        //Odradili smo checking checkboxa koji nam pokazuje "nevidljive kontakte"
        mShowInvisibleControl.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "mShowInvisibleControl changed: " + isChecked);
                mShowInvisible = isChecked;
                populateContactList();
            }
        });

        // Populate the contact list
        populateContactList();
    }

    /**
     * Populate the contact list based on account currently selected in the account spinner.
     */
    private void populateContactList() {
        // Build adapter with contact entries
        Cursor cursor = getContacts();
        String[] fields = new String[] {
                ContactsContract.Data.DISPLAY_NAME
        };
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.contact_entry, cursor,
                fields, new int[] {R.id.contactEntryText});
        mContactList.setAdapter(adapter);
    }

    /**
     * Obtains the contact list for the currently selected account.
     *
     * @return A cursor for for accessing the contact list.
     */
    private Cursor getContacts()
    {
        // Run query
        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        String[] projection = new String[] {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME
        };
        String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '" +
                (mShowInvisible ? "0" : "1") + "'";
        String[] selectionArgs = null;
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

        return managedQuery(uri, projection, selection, selectionArgs, sortOrder);
    }

    /**
     * Launches the ContactAdder activity to add a new contact to the selected accont.
     */
    /*protected void launchContactAdder() {
        Intent i = new Intent(this, ContactAdder.class);
        startActivity(i);
    }*/
}
