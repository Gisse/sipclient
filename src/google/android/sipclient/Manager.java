package google.android.sipclient;

import java.text.ParseException;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.sip.SipAudioCall;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;
import android.net.sip.SipRegistrationListener;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class Manager extends Activity{

    private static final int CALL_ADDRESS = 1;
    private static final int SET_AUTH_INFO = 2;
    private static final int UPDATE_SETTINGS_DIALOG = 3;
    private static final int HANG_UP = 4;
    
    private static final String TAG = "sipclient";
	public String sipAddress = null;

    public SipManager manager = null;
    public SipProfile me = null;
    public SipAudioCall call = null;
	private String username, domain, password;
	SharedPreferences prefs;
	public IncomingCallReceiver callReceiver;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "Manager on create");
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.sipclient.INCOMING_CALL");
        callReceiver = new IncomingCallReceiver();
        this.registerReceiver(callReceiver, filter);
        initializeManager(); 
	}
	public void startingManager(){
		Log.v(TAG, "Manager on create");
        /*IntentFilter filter = new IntentFilter();
        filter.addAction("android.sipclient.INCOMING_CALL");
        callReceiver = new IncomingCallReceiver();
        this.registerReceiver(callReceiver, filter);*/
        initializeManager(); 
	}
	
	@Override
    public void onStart() {
        super.onStart();
        // When we get back from the preference setting Activity, assume
        // settings have changed, and re-login with new auth info.
        initializeManager();
    }
	
	
	public void initializeManager() {
        if(manager == null) {
        	Log.v(TAG, "SipManager not inicialized starting SipManager");
          manager = SipManager.newInstance(getBaseContext());
        }else{
        	Log.v(TAG, "SipManager already inicialized, do nothing");
        }
        if(manager == null){
        	Log.v(TAG, "SipManager still null API not suporting SIP");
        }
        initializeLocalProfile();
    }
	
	
	/**
     * Logs you into your SIP provider, registering this device as the location to
     * send SIP calls to for your SIP address.
     */
    public void initializeLocalProfile() {
    	Log.v(TAG, "initializing loval profile");
        if (manager == null) {
        	Log.v(TAG, "SipManager is null somehow, closing and returning");
            return;
        }

        if (me != null) {
        	Log.v(TAG, "SipProfile is not null closing local profile");
            closeLocalProfile();
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String username = prefs.getString("namePref", "");
        String domain = prefs.getString("domainPref", "");
        String password = prefs.getString("passPref", "");

        if (username.length() == 0 || domain.length() == 0 || password.length() == 0) {
        	Log.v(TAG, "Radimo update profila posto su podesavanja na nuli");
            showDialog(UPDATE_SETTINGS_DIALOG);
            return;
        }

        try {
            SipProfile.Builder builder = new SipProfile.Builder(username, domain);
            builder.setPassword(password);
            me = builder.build();
            
            Intent i = new Intent();
            i.setAction("android.sipclient.INCOMING_CALL");
            PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, Intent.FILL_IN_DATA);
            manager.open(me, pi, null);


            // This listener must be added AFTER manager.open is called,
            // Otherwise the methods aren't guaranteed to fire.

            manager.setRegistrationListener(me.getUriString(), new SipRegistrationListener() {
                    public void onRegistering(String localProfileUri) {
                        updateStatus("Registering with SIP Server...");
                    }

                    public void onRegistrationDone(String localProfileUri, long expiryTime) {
                        updateStatus("Ready");
                    }

                    public void onRegistrationFailed(String localProfileUri, int errorCode,
                            String errorMessage) {
                        updateStatus("Registration failed.  Please check settings.");
                    }
                });
        } catch (ParseException pe) {
            updateStatus("Connection Error.");
        } catch (SipException se) {
            updateStatus("Connection error.");
        }
    }
	/*
	public void startManager(String username, String domain, String password){
		initializeManager();
		Log.v("sip_test", "manager: " + mSipManager.toString());
		Log.v("sip_test", "isApiSupported: " + new Boolean(mSipManager.isApiSupported(this)).toString());
		Log.v("sip_test", "isSipWifiOnly: " + new Boolean(mSipManager.isSipWifiOnly(this)).toString());
		Log.v("sip_test", "isVoipSupported: " + new Boolean(mSipManager.isVoipSupported(this)).toString());
		getPreferences();
		SipProfile.Builder builder = null;
		try {
			builder = new SipProfile.Builder(username, domain);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		builder.setPassword(password);
		mSipProfile = builder.build();
		
		Intent i = new Intent();
        i.setAction("android.sipclient.INCOMING_CALL");
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, Intent.FILL_IN_DATA);
        try {
			mSipManager.open(me, pi, null);
			mSipManager.setRegistrationListener(me.getUriString(), new SipRegistrationListener() {
                public void onRegistering(String localProfileUri) {
                    updateStatus("Registering with SIP Server...");
                }

                public void onRegistrationDone(String localProfileUri, long expiryTime) {
                    updateStatus("Ready");
                }

                public void onRegistrationFailed(String localProfileUri, int errorCode,
                        String errorMessage) {
                    updateStatus("Registration failed.  Please check settings.");
                }
            });
		} catch (SipException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}*/

	/*private void getPreferences() {
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
        username = prefs.getString("namePref", "");
        domain = prefs.getString("domainPref", "");
        password = prefs.getString("passPref", "");
        Log.v(TAG, "Username: "+username+" Domain: "+domain+" Password: "+password);
		// TODO Auto-generated method stub
		
	}*/
/*
	public void initializeManager() {
		if(mSipManager == null) {
		    mSipManager = SipManager.newInstance(this);
		}

        //initializeLocalProfile();
    }
	public SipManager getSipManager() {
		// TODO Auto-generated method stub
		return mSipManager;
	}*/
    
    /**
     * Updates the status box at the top of the UI with a messege of your choice.
     * @param status The String to display in the status box.
     */
    public void updateStatus(final String status) {
        // Be a good citizen.  Make sure UI changes fire on the UI thread.
        /*this.runOnUiThread(new Runnable() {
            public void run() {
                TextView labelView = (TextView) findViewById(R.id.sipLabel);
                labelView.setText(status);
            }
        });*/
    }
    
    /**
     * Closes out your local profile, freeing associated objects into memory
     * and unregistering your device from the server.
     */
    public void closeLocalProfile() {
    	Log.v(TAG, "closeLocalProfile zatvaramo profil");
        if (manager == null) {
            return;
        }
        try {
            if (me != null) {
                manager.close(me.getUriString());
            }
        } catch (Exception ee) {
            Log.d("WalkieTalkieActivity/onDestroy", "Failed to close local profile.", ee);
        }
    }
    
    /**
     * Make an outgoing call.
     */
    /*public void initiateCall() {

        updateStatus(sipAddress);

        try {
            SipAudioCall.Listener listener = new SipAudioCall.Listener() {
                // Much of the client's interaction with the SIP Stack will
                // happen via listeners.  Even making an outgoing call, don't
                // forget to set up a listener to set things up once the call is established.
                @Override
                public void onCallEstablished(SipAudioCall call) {
                    call.startAudio();
                    call.setSpeakerMode(true);
                    call.toggleMute();
                    //updateStatus(call);
                }

                @Override
                public void onCallEnded(SipAudioCall call) {
                    updateStatus("Ready.");
                }
            };

            call = manager.makeAudioCall(me.getUriString(), sipAddress, listener, 30);

        }
        catch (Exception e) {
            Log.i("WalkieTalkieActivity/InitiateCall", "Error when trying to close manager.", e);
            if (me != null) {
                try {
                    manager.close(me.getUriString());
                } catch (Exception ee) {
                    Log.i("WalkieTalkieActivity/InitiateCall",
                            "Error when trying to close manager.", ee);
                    ee.printStackTrace();
                }
            }
            if (call != null) {
                call.close();
            }
        }
    }*/
	
}
