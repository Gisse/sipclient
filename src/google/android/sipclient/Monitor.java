package google.android.sipclient;

import java.text.ParseException;

import google.android.sipclient.IncomingCallReceiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.sip.SipAudioCall;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;
import android.net.sip.SipRegistrationListener;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import android.media.MediaPlayer;
import android.util.Log;

public class Monitor extends Service{
	private static final String TAG = "sipclient";
	public IncomingCallReceiver callReceiver;
	public SipManager sipmanager = null;
	public SipProfile me = null;
	private String username, domain, password;
	Manager manager;
	SharedPreferences prefs;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		Toast.makeText(this, "My Service Created", Toast.LENGTH_LONG).show();
		Log.v(TAG, "onCreate");
		//initializeManager();
		//initializeManager();

	}

	@Override
	public void onDestroy() {
		Toast.makeText(this, "My Service Stopped", Toast.LENGTH_LONG).show();
		Log.v(TAG, "onDestroy");
	}
	
	@Override
	public void onStart(Intent intent, int startid) {
		Toast.makeText(this, "My Service Started", Toast.LENGTH_LONG).show();
		Log.v(TAG, "Monitor onStart starting call receiver");
		callReceiver = new IncomingCallReceiver();
        
	}    
}
