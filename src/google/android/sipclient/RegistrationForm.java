package google.android.sipclient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import org.xmlpull.v1.XmlSerializer;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegistrationForm /*extends Activity*/ extends PreferenceActivity{
	
	private static final String TAG = "sipclient";
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        // Note that none of the preferences are actually defined here.
        // They're all in the XML file res/xml/preferences.xml.
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
	
	/*private static final String TAG = "RegistracionaForma LOG ";
	private static final int EMPTY_FIELDS = 1;
	private static final int PASSWORD_MISSMATCH = 2;
	
	Context context=this;
	
	User korisnik;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registracija);
    

	 final Button button = (Button) findViewById(R.id.registration_button);
     button.setOnClickListener(new View.OnClickListener() {
         public void onClick(View v) {
        	 Log.v(TAG, "Registration button was clicked");
        	 EditText userEdit   = (EditText) findViewById(R.id.registracija_ime);
        	 EditText passwordEdit = (EditText) findViewById(R.id.registracija_password);
        	 EditText repetePasswordEdit = (EditText) findViewById(R.id.registracija_repete_password);
        	 //provera forme
        	 if(formValidation(userEdit, passwordEdit, repetePasswordEdit)){
        		 Log.v(TAG, "Form validation passed");
        		 //registrujemo na server novog korisnika
        		 if(registeringOnServer()){
        			 Log.v(TAG, "Registration on server passed");
        			 //upisujemo lokalno 
        			 if(registeringLocaly()){
        				 Log.v(TAG, "Local registration passed");
        			 }
        		 }
        	 }
             // Perform action on click
         }
         
         //Validacija forme da li imamo praznih redova ili da sesifre ne poklapaju
		private boolean formValidation(EditText username, EditText password, EditText passwordRepete) {
			// TODO Auto-generated method stub
			Log.v(TAG, "Ime:"+username.getText().toString());
			Log.v(TAG, "Sifra:"+password.getText().toString());
			Log.v(TAG, "Ponovljena sifra:"+passwordRepete.getText().toString());
			if(!(username.getText().toString().equalsIgnoreCase(""))&&!(password.getText().toString().equalsIgnoreCase(""))&&!(passwordRepete.getText().toString().equalsIgnoreCase(""))){
				Log.v(TAG, "Deo 1");
				if(password.getText().toString().equals(passwordRepete.getText().toString())){
					Log.v(TAG, "Deo 2");
					korisnik = new User();
					korisnik.username = username.getText().toString();
					korisnik.password = password.getText().toString();
					return true;
				}else{
					Log.v(TAG, "Deo 3");
					buildDialog(PASSWORD_MISSMATCH);
					return false;
				}			
			}else{
				Log.v(TAG, "Deo 4");
				buildDialog(EMPTY_FIELDS);
				return false;
			}
		}

		//Pravimo popup koji nas obavestava o gresci
		private void buildDialog(int reason) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Greska");
			if (reason == EMPTY_FIELDS){
				builder.setMessage("Proverite da li ste uneli podatke u sva polja");
			}else if (reason == PASSWORD_MISSMATCH){
				builder.setMessage("Niste pravilno ponovili sifru");	
			}
			builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			   public void onClick(DialogInterface dialog, int which) {
				   dialog.dismiss();
			   }
			});
			builder.show();
			// TODO Auto-generated method stub			
		}
		
		//Lokalno registrovanje se zasniva na sazdavanju registracionog fajla u kome se cuva password i ime korisnika
		private boolean registeringLocaly() {
			
			ContextWrapper cw = new ContextWrapper(context);
			boolean mExternalStorageAvailable = false;
			boolean mExternalStorageWriteable = false;
			String state = Environment.getExternalStorageState();

			if (Environment.MEDIA_MOUNTED.equals(state)) {
			    // We can read and write the media
			    mExternalStorageAvailable = mExternalStorageWriteable = true;
			 // create a File object for the parent directory
			    File wallpaperDirectory = new File("/sdcard/sipclient/");
			    // have the object build the directory structure, if needed.
			    wallpaperDirectory.mkdirs();
			    // create a File object for the output file
			    File outputFile = new File(wallpaperDirectory, "SIP.xml");
			    // now attach the OutputStream to the file object, instead of a String representation
			    FileOutputStream fileos = null;
				try {
					fileos = new FileOutputStream(outputFile);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					Log.v(TAG, "Glupa greska");
					e1.printStackTrace();
				}
				//we create a XmlSerializer in order to write xml data
		        XmlSerializer serializer = Xml.newSerializer();
		        try {
		        	serializer.setOutput(fileos, "UTF-8");
	                //Write <?xml declaration with encoding (if encoding not null) and standalone flag (if standalone not null)
	                //serializer.startDocument(null, Boolean.valueOf(true));
	                //start a tag called "root"
	                Log.v(TAG, "Startujemo tag root");
	                serializer.startTag(null, "root");
	                //i indent code just to have a view similar to xml-tree
	                Log.v(TAG, "Startujemo tag username");
	                        serializer.startTag(null, "username");
	                        Log.v(TAG, "popunjavamo username sa "+korisnik.username);
	                        serializer.text(korisnik.username);
	                        serializer.endTag(null, "username");
	                        Log.v(TAG, "Zakrili smo tag username");
	                        Log.v(TAG, "Startujemo tag password");
	                        serializer.startTag(null, "password");
	                        Log.v(TAG, "popunjavamo tag password sa "+korisnik.password);
	                        serializer.text(korisnik.password);
	                        serializer.endTag(null, "password");
	                        Log.v(TAG, "Zatvorili tag password");

	                serializer.endTag(null, "root");
	                Log.v(TAG, "Zatvorili tag root");
	                serializer.endDocument();
	                Log.v(TAG, "Zatvorili dokument");
	                //write xml data into the FileOutputStream
	                serializer.flush();
	                //finally we close the file stream
	                fileos.close();
	        
		        } catch (Exception e) {
		        	e.printStackTrace();
	                Log.e("Exception","error occurred while creating xml file");
	                Log.v(TAG, "Greska 3");
		        }
		        korisnik.password = null;
		        korisnik.username = null;
			} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			    // We can only read the media
			    mExternalStorageAvailable = true;
			    mExternalStorageWriteable = false;
			} else {
			    // Something else is wrong. It may be one of many other states, but all we need
			    //  to know is we can neither read nor write
			    mExternalStorageAvailable = mExternalStorageWriteable = false;
			}
			
			return true;
			
		}

		private boolean registeringOnServer() {
			// TODO Auto-generated method stub
			return true;
		}
     });
	}*/
}
