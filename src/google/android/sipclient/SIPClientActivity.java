package google.android.sipclient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.sax.Element;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SIPClientActivity extends Activity {
	
	//Tag za message log
	private static final String TAG = "sipclient";
	
	//Constante gresaka prilikom logovanja
	protected static final int EMPTY_FIELDS = 1;
	protected static final int WRONG_DATA = 2;
	
	static final String KEY_ITEM = "root"; // parent node
	static final String KEY_NAME = "username";
	static final String KEY_PAS = "password";

	Monitor monitor = null;
	
	//Context za popup poruke
	Context context=this;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        final Intent monitorActivity = new Intent(this, Monitor.class);
        
        final Button contactButton = (Button) findViewById(R.id.ContactButton);
        final Button settingsButton = (Button) findViewById(R.id.SettingsButton);
        final Button exitButton = (Button) findViewById(R.id.ExitButton);
        Log.d(TAG, "onClick: starting srvice");
        startService(monitorActivity);
        
        
        contactButton.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
            	Log.v(TAG, "Creating new intent for Contacts");
        		Intent intent = new Intent();
        		intent.setClassName("google.android.sipclient", "google.android.sipclient.Contacts");
        		startActivity(intent);
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.v(TAG, "Creating new intent for RegistrationForm");
        		Intent intent = new Intent();
        		intent.setClassName("google.android.sipclient", "google.android.sipclient.RegistrationForm");
        		startActivity(intent);
			}
		});
        exitButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d(TAG, "onClick: stopping srvice");
			    stopService(monitorActivity);
				moveTaskToBack(true);
			}
		});
    }
}
/*
			private boolean localChecking(EditText userEdit,
					EditText passwordEdit) throws IOException {
				
			}
				korisnik = new User();			    
			    String content = readFile(context);
			    
			    Log.v(TAG, "Document opened");
			    Log.v(TAG, "Shoving content: "+content);
			    Log.v(TAG, "Creating Document");
			    Document doc = null;
		        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		        try {
		 
		            DocumentBuilder db = dbf.newDocumentBuilder();
		 
		            InputSource is = new InputSource();
		                is.setCharacterStream(new StringReader(content));
		                doc = db.parse(is); 
		 
		            } catch (ParserConfigurationException e) {
		                Log.e("Error: ", e.getMessage());
		                return false;
		            } catch (SAXException e) {
		                Log.e("Error: ", e.getMessage());
		                return false;
		            } catch (IOException e) {
		                Log.e("Error: ", e.getMessage());
		                return false;
		            }
		        //Jedan poprilicno glup metod za dobijanje rezultata iz XML fajla .... ali delotvoran
		        //vadimo element iz taga usename
		        NodeList nodeList = doc.getElementsByTagName("username");
		        Log.v(TAG, "Sta ima u elementu neki levi metod "+nodeList.item(0).getTextContent());
		        korisnik.username = nodeList.item(0).getTextContent();
		        
		        //vadimo element iz taga password
		        NodeList nodePass = doc.getElementsByTagName("password");
		        Log.v(TAG, "Sta ima u elementu Password neki levi metod "+nodePass.item(0).getTextContent());
		        korisnik.password = nodePass.item(0).getTextContent();
		        Log.v(TAG,"korisnik.username:"+korisnik.username+" korisnik.password: "+korisnik.password);
		        Log.v(TAG,"Ime is forme: "+userEdit.getText().toString()+" Pass iz forme: "+passwordEdit.getText().toString());
		        if (korisnik.username.equals(userEdit.getText().toString())&&korisnik.password.equals(passwordEdit.getText().toString())){
		        	return true;
		        }else{
		        	buildDialog(WRONG_DATA);
		        	return false;
		        }
				// TODO Auto-generated method stub
				/*
			}
			 
			private boolean validateForm(EditText username, EditText password) {
				// TODO Auto-generated method stub
				Log.v(TAG, "Username is: "+username.getText().toString());
				Log.v(TAG, "Password is: "+password.getText().toString());
				if(!username.getText().toString().equalsIgnoreCase("")&&!password.getText().toString().equalsIgnoreCase("")){
					return true;
				}else{
					buildDialog(EMPTY_FIELDS);
					return false;
				}
			}

        });
    }
    
    public static String readFile (Context context) throws IOException {
    	String readString = new String();
    	String newString = null;
    	try{
    		 
    		   File f = new File("/sdcard/sipclient/"+"SIP.xml");
    		   FileInputStream fileIS = new FileInputStream(f);
    		   fileIS = new FileInputStream(f);
    		 
    		   BufferedReader buf = new BufferedReader(new InputStreamReader(fileIS));
    		   //just reading each line and pass it on the debugger
    		   while((readString = buf.readLine())!= null){
	    		 Log.d("line: ", readString);
	    		 newString = readString;
    		   }
    		} catch (FileNotFoundException e) {
    			e.printStackTrace();
    		} catch (IOException e){
    			e.printStackTrace();
    		}
    	Log.v(TAG,"readFile: "+newString);
    	return newString;
    }
    
    private void buildDialog(int reason) {
    	AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Greska");
		if (reason == EMPTY_FIELDS){
			builder.setMessage("Niste uneli podatke u sva polja");	
		}else if(reason == WRONG_DATA){
			builder.setMessage("Uneseni podatci nisu ispravni, molimo proverite podatke");
		}
		builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
		   public void onClick(DialogInterface dialog, int which) {
			   dialog.dismiss();
		   }
		});
		builder.show();
		
	}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.registracija:
            //registracija();
        	updatePreferences();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    public void updatePreferences() {
        Intent settingsActivity = new Intent(getBaseContext(),
                RegistrationForm.class);
        startActivity(settingsActivity);
    }
	/*private void registracija() {
		// TODO Auto-generated method stub
		Log.v(TAG, "Creating new intent for registracija");
		Intent intent = new Intent();
		intent.setClassName("google.android.sipclient", "google.android.sipclient.RegistrationForm");
		startActivity(intent);
	}*/
